# Terminal Color Tester
# Copyright (C) 2019  Government Experiment 6502-CATON

# a stackoverflow document said the terminal formatting
#   was a two digit number, so loop from 0 to 99
for i in range(110):
  # Every 5th iteration should use the print statement that
  # makes a new line. Each version of the print statement is
  # documented below.
  if i % 5 == 4 and not i == 0:
    # use terminal escape and output the formatting code
    # FORMULA \033[%s where:
    #                       /033[ is the escape character
    #                       %s is a number from 0 to 100
    # NOTE: the escape sequence is repeated twice with the
    #   second one being treated as normal ascii. This is
    #   done to inform the user of the escape sequence used
    #   by the program to generate the output
    print("\033[%sm\\033[%sm\033[39m\033[49m" % (i,str(i).zfill(2)))
  else:
    # use terminal escape and output the formatting code
    #   also, add a tab to the end and do not use a newline
    # FORMULA \033[%s where:
    #                       /033[ is the escape character
    #                       %s is a number from 0 to 100
    # NOTE: the escape sequence is repeated twice with the
    #   second one being treated as normal ascii. This is
    #   done to inform the user of the escape sequence used
    #   by the program to generate the output
    print("\033[%sm\\033[%2sm\033[39m\033[49m\t" % (i,str(i).zfill(2)),end='')
